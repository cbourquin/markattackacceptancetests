import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TeacherReport {
    private HtmlUnitDriver driver;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    }

    @Test(timeout = 30000)
    public void testTeacherReport() throws Exception {
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url=index");
        driver.findElement(By.id("pseudo")).clear();
        driver.findElement(By.id("pseudo")).sendKeys("TeacherTest");
        driver.findElement(By.id("pwd")).clear();
        driver.findElement(By.id("pwd")).sendKeys("test");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mot de passe:'])[1]/following::button[1]")).click();
        driver.findElement(By.linkText("Reporting")).click();
        driver.findElement(By.linkText("Module")).click();
        assertTrue(isElementPresent(By.id("average")));
        assertEquals(driver.findElement(By.id("average")).getText(), "Moyenne: 12.5");
        assertTrue(isElementPresent(By.id("standard_deviation")));
        assertEquals(driver.findElement(By.id("standard_deviation")).getText(), "Ecart Type: 0");
        assertTrue(isElementPresent(By.id("success_threshold")));
        assertEquals(driver.findElement(By.id("success_threshold")).getText(), "Taux de Réussite: 100%");
        assertEquals(driver.findElements(By.xpath("//table[@id='subject-report']/tbody/tr")).size(), 1);
        driver.findElement(By.linkText("Deconnexion")).click();
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}

