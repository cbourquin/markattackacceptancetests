import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.junit.Assert.*;

public class AuthenticationTeacher {
    private HtmlUnitDriver driver;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    }

    @Test (timeout = 30000)
    public void testAuthenticationTeacher() throws Exception {
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url=index");
        driver.findElement(By.id("pseudo")).clear();
        driver.findElement(By.id("pseudo")).sendKeys("TeacherTest");
        driver.findElement(By.id("pwd")).clear();
        driver.findElement(By.id("pwd")).sendKeys("test");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mot de passe:'])[1]/following::button[1]")).click();
        assertEquals("J-M Hufflen", driver.findElement(By.linkText("J-M Hufflen")).getText());
        assertEquals("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url=home", driver.getCurrentUrl());
        assertEquals("Mes Modules", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Deconnexion'])[1]/following::h2[1]")).getText());
        assertTrue(isElementPresent(By.linkText("Reporting")));
        assertTrue(isElementPresent(By.linkText("Configuration")));
        driver.findElement(By.linkText("Deconnexion")).click();
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @After
    public void tearDown() throws Exception {
//        driver.findElement(By.linkText("Deconnexion")).click();
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
